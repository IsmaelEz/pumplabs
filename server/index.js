const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.get('/', (req, res, next) => {
  res.send('Go to /api/test =D');
});

app.get('/api/test', (req, res, next) => {
  res.send({ test: 'PumpLabs =D!!!' });
});

app.listen(port, () => console.log(`Listening on port ${port}`));