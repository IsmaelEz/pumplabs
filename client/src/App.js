import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  state = { response: '' }

  componentDidMount () {
    this.callApi()
      .then(data => this.setState({ response: data.test }));
  }

  callApi = async () => {
    const response = await fetch('/api/test');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);

    return body;
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to { this.state.response }</h1>
        </header>
        <p className="App-intro">
          Se você ver PumpLabs acima, significa que está tudo ok =D
        </p>
      </div>
    );
  }
}

export default App;
