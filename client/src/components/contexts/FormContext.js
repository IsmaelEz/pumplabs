import React, { Component } from 'react';

const formContext = React.createContext();

class FormProvider extends Component {
  constructor (props) {
    super(props)
    this.state = {
      newUser: {
        name: '',
        email: '',
        age: '',
        gender: '',
        expertise: '',
        about: ''
      },
      genderDefaults: ['Male', 'Famale', 'Others'],
      skillOptions: ['Programming', 'Development', 'Design', 'Testing']
    }

    this.handlerInputChange = this.handlerInputChange.bind(this)
    this.handlerSubmit = this.handlerSubmit.bind(this)
  }

  handlerInputChange (event) {
    let value = event.target.value
    let name = event.target.name

    // let newUser = Object.assign({}, this.state.newUser)
    // newUser.name = value
    // { ...obj, objProperty: 'value' }

    this.setState(prevState => ({
      newUser: {...prevState.newUser, [name]: value}
    }))
  }

  handlerSubmit (event) {
    console.log( this.state.newUser );
    event.preventDefault()
  }

  render () {
    return (
      <formContext.Provider
        value = {{
          newUser: this.state.newUser,
          skillOptions: this.state.skillOptions,
          genderDefaults: this.state.genderDefaults,
          handlerInputChange: this.handlerInputChange,
          handlerSubmit: this.handlerSubmit
        }}
      >
        { this.props.children }
      </formContext.Provider>
    )
  }
}

const FormConsumer = formContext.Consumer;

export {
  FormProvider,
  FormConsumer
}