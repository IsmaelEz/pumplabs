import React from 'react';
import PropTypes from 'prop-types';

const Select = (props) => (
  <div className="form-group">
    <label htmlFor={ props.name }>{ props.title }</label>
    <select
      name={ props.name }
      value={ props.value }
      onChange={ props.handlerChange }
    >
      <option value="" disabled hidden>{ props.placeholder }</option>
      {props.options.map(option => (
        <option
          key={ option }
          value={ option }
          label={ option }
        > { option }
        </option>
      ))}
    </select>
  </div>
)

Select.propTypes = {
  name: PropTypes.string,
  title: PropTypes.string,
  value: PropTypes.string,
  handlerChange: PropTypes.func,
  placeholder: PropTypes.string,
  options: PropTypes.array,
}

export default Select;