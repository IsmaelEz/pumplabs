import React from 'react';
import PropTypes from 'prop-types';

const Input = (props) => (
  <div className="form-group">
    <label htmlFor={ props.name } className="form-label">{ props.title }</label>
    <input
      className="form-input"
      type={ props.type }
      id={ props.name }
      name={ props.name }
      value={ props.value }
      onChange={ props.handlerInputChange }
      placeholder={ props.placeholder }
    />
  </div>
)

Input.propTypes = {
  title: PropTypes.string,
  type: PropTypes.string,
  name: PropTypes.string,
  value: PropTypes.string,
  handlerInputChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string
}

export default Input;