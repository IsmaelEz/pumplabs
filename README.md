# PumpLabs

Everything here is gonna blow up soon, just get out of here as soon as possible!

### Starting... =D

```console
pump@labs:~$ cd client && npm i
pump@labs:~$ cd server && npm i
pump@labs:~$ npm run dev (inside server folder)
pump@labs:~$ Boooooom!
```

### To do ~ Front-End

### To do ~ Back-End

### Technologies and tools

#### Front-End

- Reactjs
- Redux
- RxJs

#### Back-End

- Nodejs
- Express
- Mongodb

#### Tools

- NPM
- Webpack

### Project structure

```
├── client
│   ├── package.json
│   ├── public
│   │   ├── favicon.ico
│   │   ├── index.html
│   │   └── manifest.json
│   ├── src
│   │   ├── App.css
│   │   ├── App.js
│   │   ├── App.test.js
│   │   ├── index.css
│   │   ├── index.js
│   │   ├── logo.svg
│   │   └── registerServiceWorker.js
│   └── yarn.lock
├── README.md
└── server
    ├── index.js
    ├── package.json
    └── package-lock.json

4 directories, 16 files
```